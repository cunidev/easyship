from django.db import models
from django.contrib.auth.models import User

class Shipment(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	name = models.TextField()
	address = models.TextField()