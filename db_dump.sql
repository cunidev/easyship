BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "django_migrations" (
	"id"	integer NOT NULL,
	"app"	varchar(255) NOT NULL,
	"name"	varchar(255) NOT NULL,
	"applied"	datetime NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "auth_group_permissions" (
	"id"	integer NOT NULL,
	"group_id"	integer NOT NULL,
	"permission_id"	integer NOT NULL,
	FOREIGN KEY("group_id") REFERENCES "auth_group"("id") DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY("permission_id") REFERENCES "auth_permission"("id") DEFERRABLE INITIALLY DEFERRED,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "auth_user_groups" (
	"id"	integer NOT NULL,
	"user_id"	integer NOT NULL,
	"group_id"	integer NOT NULL,
	FOREIGN KEY("user_id") REFERENCES "auth_user"("id") DEFERRABLE INITIALLY DEFERRED,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("group_id") REFERENCES "auth_group"("id") DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS "auth_user_user_permissions" (
	"id"	integer NOT NULL,
	"user_id"	integer NOT NULL,
	"permission_id"	integer NOT NULL,
	FOREIGN KEY("user_id") REFERENCES "auth_user"("id") DEFERRABLE INITIALLY DEFERRED,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("permission_id") REFERENCES "auth_permission"("id") DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS "django_admin_log" (
	"id"	integer NOT NULL,
	"action_time"	datetime NOT NULL,
	"object_id"	text,
	"object_repr"	varchar(200) NOT NULL,
	"change_message"	text NOT NULL,
	"content_type_id"	integer,
	"user_id"	integer NOT NULL,
	"action_flag"	smallint unsigned NOT NULL CHECK("action_flag" >= 0),
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("content_type_id") REFERENCES "django_content_type"("id") DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY("user_id") REFERENCES "auth_user"("id") DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS "django_content_type" (
	"id"	integer NOT NULL,
	"app_label"	varchar(100) NOT NULL,
	"model"	varchar(100) NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "auth_permission" (
	"id"	integer NOT NULL,
	"content_type_id"	integer NOT NULL,
	"codename"	varchar(100) NOT NULL,
	"name"	varchar(255) NOT NULL,
	FOREIGN KEY("content_type_id") REFERENCES "django_content_type"("id") DEFERRABLE INITIALLY DEFERRED,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "auth_group" (
	"id"	integer NOT NULL,
	"name"	varchar(150) NOT NULL UNIQUE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "auth_user" (
	"id"	integer NOT NULL,
	"password"	varchar(128) NOT NULL,
	"last_login"	datetime,
	"is_superuser"	bool NOT NULL,
	"username"	varchar(150) NOT NULL UNIQUE,
	"last_name"	varchar(150) NOT NULL,
	"email"	varchar(254) NOT NULL,
	"is_staff"	bool NOT NULL,
	"is_active"	bool NOT NULL,
	"date_joined"	datetime NOT NULL,
	"first_name"	varchar(150) NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "django_session" (
	"session_key"	varchar(40) NOT NULL,
	"session_data"	text NOT NULL,
	"expire_date"	datetime NOT NULL,
	PRIMARY KEY("session_key")
);
CREATE TABLE IF NOT EXISTS "easyship_shipment" (
	"id"	integer NOT NULL,
	"owner_id"	integer NOT NULL,
	"name"	text NOT NULL,
	"address"	text NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("owner_id") REFERENCES "auth_user"("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO "django_migrations" VALUES (1,'contenttypes','0001_initial','2020-10-04 17:04:36.985090');
INSERT INTO "django_migrations" VALUES (2,'auth','0001_initial','2020-10-04 17:04:37.010612');
INSERT INTO "django_migrations" VALUES (3,'admin','0001_initial','2020-10-04 17:04:37.031464');
INSERT INTO "django_migrations" VALUES (4,'admin','0002_logentry_remove_auto_add','2020-10-04 17:04:37.064536');
INSERT INTO "django_migrations" VALUES (5,'admin','0003_logentry_add_action_flag_choices','2020-10-04 17:04:37.087075');
INSERT INTO "django_migrations" VALUES (6,'contenttypes','0002_remove_content_type_name','2020-10-04 17:04:37.112014');
INSERT INTO "django_migrations" VALUES (7,'auth','0002_alter_permission_name_max_length','2020-10-04 17:04:37.126758');
INSERT INTO "django_migrations" VALUES (8,'auth','0003_alter_user_email_max_length','2020-10-04 17:04:37.142961');
INSERT INTO "django_migrations" VALUES (9,'auth','0004_alter_user_username_opts','2020-10-04 17:04:37.159990');
INSERT INTO "django_migrations" VALUES (10,'auth','0005_alter_user_last_login_null','2020-10-04 17:04:37.184277');
INSERT INTO "django_migrations" VALUES (11,'auth','0006_require_contenttypes_0002','2020-10-04 17:04:37.191826');
INSERT INTO "django_migrations" VALUES (12,'auth','0007_alter_validators_add_error_messages','2020-10-04 17:04:37.207074');
INSERT INTO "django_migrations" VALUES (13,'auth','0008_alter_user_username_max_length','2020-10-04 17:04:37.232313');
INSERT INTO "django_migrations" VALUES (14,'auth','0009_alter_user_last_name_max_length','2020-10-04 17:04:37.249016');
INSERT INTO "django_migrations" VALUES (15,'auth','0010_alter_group_name_max_length','2020-10-04 17:04:37.263986');
INSERT INTO "django_migrations" VALUES (16,'auth','0011_update_proxy_permissions','2020-10-04 17:04:37.278404');
INSERT INTO "django_migrations" VALUES (17,'auth','0012_alter_user_first_name_max_length','2020-10-04 17:04:37.294703');
INSERT INTO "django_migrations" VALUES (18,'sessions','0001_initial','2020-10-04 17:04:37.303504');
INSERT INTO "django_content_type" VALUES (1,'admin','logentry');
INSERT INTO "django_content_type" VALUES (2,'auth','permission');
INSERT INTO "django_content_type" VALUES (3,'auth','group');
INSERT INTO "django_content_type" VALUES (4,'auth','user');
INSERT INTO "django_content_type" VALUES (5,'contenttypes','contenttype');
INSERT INTO "django_content_type" VALUES (6,'sessions','session');
INSERT INTO "django_content_type" VALUES (7,'easyship','shipment');
INSERT INTO "auth_permission" VALUES (1,1,'add_logentry','Can add log entry');
INSERT INTO "auth_permission" VALUES (2,1,'change_logentry','Can change log entry');
INSERT INTO "auth_permission" VALUES (3,1,'delete_logentry','Can delete log entry');
INSERT INTO "auth_permission" VALUES (4,1,'view_logentry','Can view log entry');
INSERT INTO "auth_permission" VALUES (5,2,'add_permission','Can add permission');
INSERT INTO "auth_permission" VALUES (6,2,'change_permission','Can change permission');
INSERT INTO "auth_permission" VALUES (7,2,'delete_permission','Can delete permission');
INSERT INTO "auth_permission" VALUES (8,2,'view_permission','Can view permission');
INSERT INTO "auth_permission" VALUES (9,3,'add_group','Can add group');
INSERT INTO "auth_permission" VALUES (10,3,'change_group','Can change group');
INSERT INTO "auth_permission" VALUES (11,3,'delete_group','Can delete group');
INSERT INTO "auth_permission" VALUES (12,3,'view_group','Can view group');
INSERT INTO "auth_permission" VALUES (13,4,'add_user','Can add user');
INSERT INTO "auth_permission" VALUES (14,4,'change_user','Can change user');
INSERT INTO "auth_permission" VALUES (15,4,'delete_user','Can delete user');
INSERT INTO "auth_permission" VALUES (16,4,'view_user','Can view user');
INSERT INTO "auth_permission" VALUES (17,5,'add_contenttype','Can add content type');
INSERT INTO "auth_permission" VALUES (18,5,'change_contenttype','Can change content type');
INSERT INTO "auth_permission" VALUES (19,5,'delete_contenttype','Can delete content type');
INSERT INTO "auth_permission" VALUES (20,5,'view_contenttype','Can view content type');
INSERT INTO "auth_permission" VALUES (21,6,'add_session','Can add session');
INSERT INTO "auth_permission" VALUES (22,6,'change_session','Can change session');
INSERT INTO "auth_permission" VALUES (23,6,'delete_session','Can delete session');
INSERT INTO "auth_permission" VALUES (24,6,'view_session','Can view session');
INSERT INTO "auth_permission" VALUES (25,7,'add_shipment','Can add shipment');
INSERT INTO "auth_permission" VALUES (26,7,'change_shipment','Can change shipment');
INSERT INTO "auth_permission" VALUES (27,7,'delete_shipment','Can delete shipment');
INSERT INTO "auth_permission" VALUES (28,7,'view_shipment','Can view shipment');
INSERT INTO "auth_user" VALUES (1,'pbkdf2_sha256$216000$MrfXjuus4Sym$hbwPc/5Sz3m/2KOGe1vt9ScGz0jhzTq8pwWW/+7InE4=','2020-10-17 07:36:08.354887',1,'admin','','admin@localhost',1,1,'2020-10-05 10:10:16.915851','');
INSERT INTO "auth_user" VALUES (3,'pbkdf2_sha256$216000$7wAn2XlERqKK$jjsgG/Kd3tiH4CAXhviNa6KiEG8LA6F+I6UyBNdfWiU=','2020-10-17 07:35:35.262411',1,'bob','','bob@gmail.com.fake',1,1,'2020-10-15 19:22:01.179824','');
INSERT INTO "django_session" VALUES ('<md5 HASH object @ 0x7f7e5bc27510>','.eJxVjEEOwiAQRe_C2hCGggWX7nsGwgyDVA0kpV0Z765NutDtf-_9lwhxW0vYOi9hTuIiQJx-N4z04LqDdI_11iS1ui4zyl2RB-1yaomf18P9Oyixl2-t2CbLmmzK2Z4HAMXoMmQfHXilDREQKTeMg0GfGTWjYmci6BEMI4v3B_ElOFE:1kTgWU:SOxcnbPeQPi8-TkWuMS01uvA_hYkdWSQug42Fv8j7tY','2020-10-31 07:21:02.096198');
INSERT INTO "django_session" VALUES ('<md5 HASH object @ 0x7f7e5b3f1cc0>','.eJxVjEEOwiAQRe_C2hCGggWX7nsGwgyDVA0kpV0Z765NutDtf-_9lwhxW0vYOi9hTuIiQJx-N4z04LqDdI_11iS1ui4zyl2RB-1yaomf18P9Oyixl2-t2CbLmmzK2Z4HAMXoMmQfHXilDREQKTeMg0GfGTWjYmci6BEMI4v3B_ElOFE:1kTgWY:0_egCvDZQ3q98LmwEurSreiEW0i_lQshIU6EWNmMPZo','2020-10-31 07:21:06.604150');
INSERT INTO "django_session" VALUES ('<md5 HASH object @ 0x7f7e5b3f1d20>','.eJxVjEEOwiAQRe_C2hCGggWX7nsGwgyDVA0kpV0Z765NutDtf-_9lwhxW0vYOi9hTuIiQJx-N4z04LqDdI_11iS1ui4zyl2RB-1yaomf18P9Oyixl2-t2CbLmmzK2Z4HAMXoMmQfHXilDREQKTeMg0GfGTWjYmci6BEMI4v3B_ElOFE:1kTgWd:XECac99S9RRm2wPjq_bjrAp4Q_G54WqhgSMWLtdQQCs','2020-10-31 07:21:11.943009');
INSERT INTO "django_session" VALUES ('f35f517c887c9d79c8d8973083c94860','.eJxVjEEOwiAQRe_C2hCGggWX7nsGwgyDVA0kpV0Z765NutDtf-_9lwhxW0vYOi9hTuIiQJx-N4z04LqDdI_11iS1ui4zyl2RB-1yaomf18P9Oyixl2-t2CbLmmzK2Z4HAMXoMmQfHXilDREQKTeMg0GfGTWjYmci6BEMI4v3B_ElOFE:1kTggW:VpgGzg8DyJeU0Mr_9qk-SKx5wW56GkDVFHbVttB9Yjw','2020-10-31 07:31:24.375326');
INSERT INTO "django_session" VALUES ('6893b047079ca5955fd0419bf62d8a84','.eJxVjEEOwiAQRe_C2hCGggWX7nsGwgyDVA0kpV0Z765NutDtf-_9lwhxW0vYOi9hTuIiQJx-N4z04LqDdI_11iS1ui4zyl2RB-1yaomf18P9Oyixl2-t2CbLmmzK2Z4HAMXoMmQfHXilDREQKTeMg0GfGTWjYmci6BEMI4v3B_ElOFE:1kTgl6:q7fKL6nlH7LzEfF61039bK2OYBFfBxHGF5H_v9gvNrs','2020-10-31 07:36:08.363658');
INSERT INTO "easyship_shipment" VALUES (2,1,'iPhone 8 Rose Gold - Factory Refurbished','Mary Green, Roseland Avenue 56, 01234 New York (USA)');
INSERT INTO "easyship_shipment" VALUES (3,1,'🎃 Halloween Themed Spooky Children Book 🎃','Mary Green, Roseland Avenue 56, 01234 New York (USA)');
INSERT INTO "easyship_shipment" VALUES (7,1,'Tasty Salmon Cat Food XXL Pack, 500KG Pallet','The Cat Discount, Fairfield Road 99, 1234 York (UK)');
INSERT INTO "easyship_shipment" VALUES (8,1,'Chicken Döner Kebab Meat Roll, Entry Level, 100KG','Gordon Ramsay, 24 Downing Street, London (UK)');
CREATE UNIQUE INDEX IF NOT EXISTS "auth_group_permissions_group_id_permission_id_0cd325b0_uniq" ON "auth_group_permissions" (
	"group_id",
	"permission_id"
);
CREATE INDEX IF NOT EXISTS "auth_group_permissions_group_id_b120cbf9" ON "auth_group_permissions" (
	"group_id"
);
CREATE INDEX IF NOT EXISTS "auth_group_permissions_permission_id_84c5c92e" ON "auth_group_permissions" (
	"permission_id"
);
CREATE UNIQUE INDEX IF NOT EXISTS "auth_user_groups_user_id_group_id_94350c0c_uniq" ON "auth_user_groups" (
	"user_id",
	"group_id"
);
CREATE INDEX IF NOT EXISTS "auth_user_groups_user_id_6a12ed8b" ON "auth_user_groups" (
	"user_id"
);
CREATE INDEX IF NOT EXISTS "auth_user_groups_group_id_97559544" ON "auth_user_groups" (
	"group_id"
);
CREATE UNIQUE INDEX IF NOT EXISTS "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq" ON "auth_user_user_permissions" (
	"user_id",
	"permission_id"
);
CREATE INDEX IF NOT EXISTS "auth_user_user_permissions_user_id_a95ead1b" ON "auth_user_user_permissions" (
	"user_id"
);
CREATE INDEX IF NOT EXISTS "auth_user_user_permissions_permission_id_1fbb5f2c" ON "auth_user_user_permissions" (
	"permission_id"
);
CREATE INDEX IF NOT EXISTS "django_admin_log_content_type_id_c4bce8eb" ON "django_admin_log" (
	"content_type_id"
);
CREATE INDEX IF NOT EXISTS "django_admin_log_user_id_c564eba6" ON "django_admin_log" (
	"user_id"
);
CREATE UNIQUE INDEX IF NOT EXISTS "django_content_type_app_label_model_76bd3d3b_uniq" ON "django_content_type" (
	"app_label",
	"model"
);
CREATE UNIQUE INDEX IF NOT EXISTS "auth_permission_content_type_id_codename_01ab375a_uniq" ON "auth_permission" (
	"content_type_id",
	"codename"
);
CREATE INDEX IF NOT EXISTS "auth_permission_content_type_id_2f476e4b" ON "auth_permission" (
	"content_type_id"
);
CREATE INDEX IF NOT EXISTS "django_session_expire_date_a5c62663" ON "django_session" (
	"expire_date"
);
CREATE INDEX IF NOT EXISTS "easyship_shipment_owner_id_51a7fdcc" ON "easyship_shipment" (
	"owner_id"
);
COMMIT;
