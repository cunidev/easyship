1000w

LINK: https://gitlab.com/cunidev/easyship

EasyShip is a simple shipping management system for shipping agents, which allows to create, edit, view and search through shipments. The project has been done from the empty Django template, using the knowledge learned through the previous exercises.

Installation should not be needed, since the project is a simple Django-based web appplication which can be managed very similarly to the ones already seen. This means that it can be directly executed with the `python3 manage.py runserver` command, possibly after database migrations are done (`python3 manage.py migrate`) in order to have a consistent, up-to-date schema. 

A SQL dump of the database contents has also been attached (`db_dump.xml`) to regenerate the schema if needed (e.g. in case of SQLite corruption), and the current ("super")users are admin:admin (the one owning all the shipments currently in DB) and bob:kittens (a newly registered other user with no shipments in the database yet, which can however exploit some of the vulnerabilities below).

---

FLAW 1: SQL Injection in search form

The search form used by EasyShip currently queries the database using a raw SQL query, simply concatenating the user-provided search keyword to the SQL query without any kind of sanitization of the input string. This means that full control on the output query is possible, especially since the table listing the shippings below displays a result for the query and may easily "leak" potentially sensitive data if a malicious query is crafted.

This can be fixed by making use of Django's built-in "filter", which relies on internal sanitization of the string from most known types of attacks, rather than a simple cursor query, or by wrapping the input data to the SQL query in a "prepared statement" parameter[1] to prevent special characters from being evaluated as part of the query syntax. 

---

FLAW 2: Broken Authentication / Broken Access Control

In order "to simplify debugging", the careless developer of this system has chosen a rather predictable formula for computing the access tokens, which are simply the MD5 hash of a linear, incremental string ("session number #{id}"), where the first unused hash is chosen. This, however, can be understood within minutes by even the less experienced attacker, who will recognize and crack the MD5 within minutes through a "brute-force" attack, and will consequently be able to forge and attempt access by trying all possible session numbers incrementally. An alternative attack would also consist of creating several sessions especially in a period of low system load (to get smaller, and thus more likely recycled IDs), saving all the resulting session ids and using them ("replay attack") in a later moment to be almost sure that they correspond to some valid session in that period.

The solution to this problem could be as simple as removing the custom authentication handler and let the Django backend do the job of generating safer, randomized session IDs using its default algorithm. Alternatively, another randomized algorithm for sessions IDs could be designed, but in our case there is probably no need for that.

---

FLAW 3: Sensitive Data Exposure

In this case, sensitive data can be exposed by modifying the shipping ID in the /edit/{...}/ URL to any other ID not belonging to the user. This allows the user to see sensitive data from other accounts.

Preventive techniques are simple to implement, and consist for example in making sure that the shipment with such ID is effectively owned by the currently logged-in user before rendering the page (thus adding an "owner=" parameter to the `Shipment.objects.get` function[2]), and redirecting them to the homepage or showing an error message if no match is returned. The correct behaviour is shown in `addView`[3], where ownership of the item is checked by the query before confirming the modification.

An alternative, modern technique would be using very large, randomized hashes instead of linear, incremental IDs in order to let for example external users knowing the "secret" link also view details about some shipments.

FLAW 4: Cross-site Scripting (XSS) in homepage search form

Currently the search page is using, as is common practice, the GET request method, in order to remain in the client browser history for future reference. However, since no type of XSS prevention is done, any seemingly legit link to this page could leak logged users' cookies, data and more to attackers. 

A simple example of a malicious link could be this: http://127.0.0.1:8000/?q=%22%3E%3Cscript%3Ealert%28%22xss%22%29%3C%2Fscript%3E

The issue in this case can be fixed by letting Django sanitize the search string by removing the `{% autoescape off %}` in order to make most if not all types of XSS impossible to perform.

FLAW 5: Insufficient Logging & Monitoring

In the current design, if one follows the "install instructions" provided above, no logging is done at all in the system, for example on the HTTP requests or on the edits performed to the database tables, which could be for example reverted to look innocuous after an attack. Also a history of search queries or operations in general is not stored in a (possibly different, isolated) database, making it easier for attackers to get away without a trace.

Finally, since debug mode has been "accidentally" left activated, by default the Django web server leaks relevant implementation details (e.g. well known logical errors) to potential attackers and logs only to the standard output, which means that no logging occurs at all by default at launch time, and the smallest of issues, such as a server reboot or the end of the console buffer size, would make the on-screen log disappear.

This can be partly solved by using the Django Logger API whenever a query is done and parameters from the user are received, and partly by setting the Django logger handler to save to disk, as can be seen in the official documentation[4]. And, of course, debug mode is to be disabled in the Django settings[5] before this application goes into production.

---

[1] stmt python cursor sqlite
[2] views.py, line 38
[3] views.py, line 19
[4] https://docs.djangoproject.com/en/3.1/topics/logging/
[5] https://docs.djangoproject.com/en/3.1/ref/settings/#std:setting-DEBUG